package com.neyguvj.skillbranchtest.layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neyguvj.skillbranchtest.AppConfig;
import com.neyguvj.skillbranchtest.R;
import com.neyguvj.skillbranchtest.adapters.CharacterAdapter;
import com.neyguvj.skillbranchtest.models.CharacterInfo;
import com.neyguvj.skillbranchtest.network.CharacterService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CharacterListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class CharacterListFragment extends Fragment {

    public CharacterListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_character_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final RecyclerView recyclerView = (RecyclerView) getView().findViewById(R.id.character_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CharacterService service = retrofit.create(CharacterService.class);
        Call<List<CharacterInfo>> characters = service.getCharacters();
        characters.enqueue(new Callback<List<CharacterInfo>>() {
            @Override
            public void onResponse(Call<List<CharacterInfo>> call, Response<List<CharacterInfo>> response) {
                List<CharacterInfo> characterInfoList = response.body();
                CharacterAdapter adapter = new CharacterAdapter(characterInfoList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<CharacterInfo>> call, Throwable t) {

            }
        });
    }
}
