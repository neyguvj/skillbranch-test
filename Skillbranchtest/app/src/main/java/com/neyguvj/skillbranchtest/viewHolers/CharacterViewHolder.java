package com.neyguvj.skillbranchtest.viewHolers;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.neyguvj.skillbranchtest.R;

/**
 * Created by golov-rg on 18.10.16.
 */

public class CharacterViewHolder extends RecyclerView.ViewHolder {

    private final ImageView mIconView;
    private final TextView mNameView;

    public CharacterViewHolder(View itemView) {
        super(itemView);
        mIconView = (ImageView) itemView.findViewById(R.id.house_icon);
        mNameView = (TextView) itemView.findViewById(R.id.character_name);
    }

    public void setIcon(int id) {
        Resources resources = mIconView.getResources();
        Drawable drawable = resources.getDrawable(id);
        mIconView.setImageDrawable(drawable);
    }

    public void setName(String name) {
        mNameView.setText(name);
    }
}
