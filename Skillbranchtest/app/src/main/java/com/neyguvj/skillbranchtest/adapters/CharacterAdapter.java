package com.neyguvj.skillbranchtest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neyguvj.skillbranchtest.R;
import com.neyguvj.skillbranchtest.models.CharacterInfo;
import com.neyguvj.skillbranchtest.viewHolers.CharacterViewHolder;

import java.util.List;

/**
 * Created by golov-rg on 18.10.16.
 */

public class CharacterAdapter extends RecyclerView.Adapter<CharacterViewHolder> {
    private final List<CharacterInfo> mCharacters;

    public CharacterAdapter(List<CharacterInfo> characters) {
        mCharacters = characters;
    }


    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.character_item, parent, false);
        return new CharacterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        CharacterInfo info = mCharacters.get(position);
        holder.setName(info.getName());
        holder.setIcon(R.drawable.lanister_icon);
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }
}
