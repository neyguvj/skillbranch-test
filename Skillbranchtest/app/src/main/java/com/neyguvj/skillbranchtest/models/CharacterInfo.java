package com.neyguvj.skillbranchtest.models;

/**
 * Created by golov-rg on 18.10.16.
 */

public class CharacterInfo {

    private String url;
    private String name;
    private String culture;
    private String born;
    private String died;
    private String father;
    private String mother;
    private String spouse;

    private String[] titles;
    private String[] aliases;
    private String[] allegiances;
    private String[] books;
    private String[] povBooks;
    private String[] tvseries;

    public String getUrl() {
        return url;
    }

    public String getCulture() {
        return culture;
    }

    public String getBorn() {
        return born;
    }

    public String getDied() {
        return died;
    }

    public String getFather() {
        return father;
    }

    public String getMother() {
        return mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public String[] getTitles() {
        return titles;
    }

    public String[] getAliases() {
        return aliases;
    }

    public String[] getAllegiances() {
        return allegiances;
    }

    public String[] getBooks() {
        return books;
    }

    public String[] getPovBooks() {
        return povBooks;
    }

    public String[] getTvseries() {
        return tvseries;
    }

    public String getName() {
        return name;
    }
}
