package com.neyguvj.skillbranchtest.network;

import com.neyguvj.skillbranchtest.models.CharacterInfo;
import com.neyguvj.skillbranchtest.models.HouseInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by golov-rg on 18.10.16.
 */

public interface CharacterService {

    @GET("characters/{character}")
    Call<CharacterInfo> getCharacter(@Path("character") String character);

    @GET("characters/")
    Call<List<CharacterInfo>> getCharacters();

    @GET("houses/")
    Call<List<HouseInfo>>getHouses();

}
