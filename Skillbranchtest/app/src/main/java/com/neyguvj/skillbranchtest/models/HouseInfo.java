package com.neyguvj.skillbranchtest.models;

/**
 * Created by golov-rg on 18.10.16.
 */

public class HouseInfo {
    private String url;
    private String name;
    private String region;
    private String coatOfArms;
    private String words;
    private String[] titles;
    private String[] seats;
    private String currentLord;
    private String heir;
    private String overlord;
    private String founded;
    private String founder;
    private String diedOut;
    private String[] ancestralWeapons;
    private String[] cadetBranches;
    private String[] swornMembers;

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCoatOfArms() {
        return coatOfArms;
    }

    public String getWords() {
        return words;
    }

    public String[] getTitles() {
        return titles;
    }

    public String[] getSeats() {
        return seats;
    }

    public String getCurrentLord() {
        return currentLord;
    }

    public String getHeir() {
        return heir;
    }

    public String getOverlord() {
        return overlord;
    }

    public String getFounded() {
        return founded;
    }

    public String getFounder() {
        return founder;
    }

    public String getDiedOut() {
        return diedOut;
    }

    public String[] getAncestralWeapons() {
        return ancestralWeapons;
    }

    public String[] getCadetBranches() {
        return cadetBranches;
    }

    public String[] getSwornMembers() {
        return swornMembers;
    }
}
