package com.neyguvj.skillbranchtest.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.neyguvj.skillbranchtest.layout.CharacterListFragment;
import com.neyguvj.skillbranchtest.models.HouseInfo;

import java.util.List;

/**
 * Created by golov-rg on 18.10.16.
 */

public class HouseViewPagerAdapter extends FragmentPagerAdapter {
    private final List<HouseInfo> mHouses;

    public HouseViewPagerAdapter(FragmentManager fragmentManager, List<HouseInfo> houses) {
        super(fragmentManager);
        mHouses = houses;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new CharacterListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return mHouses.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mHouses.get(position).getName();
    }
}
